			Edit
			A simple text editor
			by Thomas Leonard

Version 2.1

Babyfai Cheung (1):
      Update Chinese Translations

Stephen Watson (1):
      Work around change in Python 2.5

Thomas Leonard (19):
      Added feed-for
      Name required Python modules in help text
      Now that the search minibuffer doesn't show the current search direction, changing direction
      When loading a file by drag and drop, load to the place the user dropped the file, not to the cursor (Thomas Leonard and Ken Hayber)
      Dragging to the toolbar opens the file in a new window instead of inserting it (Ken Hayber and Thomas Leonard)
      Added 'Search Again' function (Thomas Leonard and Ken Hayber)
      Minor code cleanup
      Aim for a default width of around 100 characters (suggested by Tristan)
      Updated Spanish translation (Juan Carlos Jiménez)
      If turning on spell checking doesn't work, report the error
      Make Edit work without gtksourceview again (Guido Schimmels)
      Updated local Zero Install interface to newer format
      Updated tests to use Python2.4 and to specify the encoding
      Set the transient_for hint on saveboxes (Thomas Leonard)
      Fix for Python 2.6: "with" is now a keyword
      Allow releasing using 0release
      Allow running unit-tests with 0test


Version 2.0 and before


08-Dec-2005

	Added 'Search Again' function (Thomas Leonard and Ken Hayber).
	Pressing Return during a search now closes the minibuffer; use
	the down arrow to get to the next match (Thomas Leonard).
	Bugfix: The goto minibuffer would no longer close (Thomas Leonard).

06-Dec-2005

	Now that the search minibuffer doesn't show the current search 
	direction, changing direction automatically moves to the next match 
	(Thomas Leonard).
	Pressing Escape always closes the minibuffer, even if the focus isn't in 
	it (Thomas Leonard).
	When loading a file by drag and drop, load to the place the user 
	dropped the file, not to the cursor (Thomas Leonard and Ken Hayber).
	Dragging to the toolbar opens the file in a new window instead of 
	inserting it (Ken Hayber and Thomas Leonard).

20-Sep-2005 (Release 2.0)

	Bugfix: Indenting Python code in spaces-for-tabs mode failed (Thomas Leonard).

03-Sep-2005

	Parsing the line number option requires Python 2.3. On Python 2.2, just
	disable this instead of aborting. As an alternative, allow the syntax
	file:number (Thomas Leonard).

27-Aug-2005

	Added Edit.xml file, which can be used as a feed for 0launch (Thomas Leonard).

07-Jul-2005

	Added tooltips for the GtkSourceView options (Thomas Leonard).

22-Jun-2005

	When gtksourceview is missing, shade those options and explain why they
	don't work (Thomas Leonard).

06-Jun-2005

	When saving a file with no name, don't show Discard (Thomas Leonard).

31-May-2005

	Guido Schimmels:
	Add GTK_SPELL_CHECK ToggleButton to toolbar when pygtkspell is available.

30-May-2005

	Guido Schimmels:
	Increase indendation level after python statement (previous line ends with colon)

29-May-2005

	Updated Chinese translations (Babyfai Cheung).

28-May-2005

	Add missing marker.png image

	Ken Hayber:

	Switch to basedir spec
	Code cleanups

25-May-2005

	Ken Hayber:

	New CLI option: -l (--line_number) -> jump to given line number at startup

21-May-2005

	Added Save toolbar button, renaming the old Save to Save As (Guido Schimmels).

20-May-2005

	Ken Hayber:

	Make Find minibuffer work like Firefox's.
	Indent/unindent of blocks (select a block and tab/shift-tab)
	Fix a warning when trying to undo too far and places the cursor 
	at the undo/redo point (so you can see what you are un/redoing).

16-May-2005

	Ken Hayber + Guido Schimmels:

	Make <Untitled> translatable.
	Prettify search/replace dialog with sizegroup.
	Options.xml: move Toolbar frame at the end of Display section, remove Toolbar
	section, line up Display->Colour->Foreground/Background in a hbox.
	Update de.po.
	Use gtksourceview's undo/redo system (it groups edits together so 
	if you type a word and hit undo it kills the whole word at once).
	Fix g.FALSE/TRUE deprecation warnings.
	Fix a small bug/warning about self.dir not being defined in search.py.
	Set the mime-type in the SaveBox from the file type if possible.
	Add support for bookmarks (set, clear and jump to).
	Make the search minibuffer work/look more like FireFox's search.

12-May-2005

	Updated Chinese translation (Babyfai Cheung).

03-May-2005

	Added support for GtkSourceView (Guido Schimmels, with some untested changes
	by Thomas Leonard).
	Updated findrox.

03-Mar-2005

	Renamed 'Save' to 'Save As' for consistency with other systems (Thomas
	Leonard).

19-Feb-2005

	Temporary fix for deprecation warnings with pygtk 2.6 (Stephen Watson).

12-Feb-2005

	Updated Chinese translation (Babyfai Cheung).

29-Jan-2005 (Release 1.9.7)

	Added some unit tests for search and replace.
	Bugfix: Search and Replace failed to correctly modify lines containing
	non-ASCII characters (Thomas Leonard; reported by Arnaud Calvo).

23-Dec-2004

	Added Chinese translation (Babyfai Cheung).

15-Jul-2004

	Added option to toggle display of toolbar (requested by JoeHill).

18-Apr-2004 (Release 1.9.6)

	Added Word Wrap menu item and toolbar toggle (Ken Hayber).

18-Mar-2004

	Added German translation (Guido Schimmels).

22-Feb-2004 (Release 1.9.5)

	Record size and mtime of file when editing starts, so newer ROX-Libs can
	warn about other applications editing the file.

27-Jan-2004

	Added French translation (Arnaud Calvo).

13-Jan-2004

	Option to turn word wrapping on and off (Ken Hayber).

17-Oct-2003

	Bugfix: Needed to 'import os' for Python 2.2 (Peter Geer).

14-Oct-2003 (Release 1.9.4)

	Added toolbar icon for Show Changes.
	Added line number display, which also shows details of the current selection,
	if any (requested by Arnaud Calvo).

13-Oct-2003

	Added ability to bring up a diff of the changes to the saved version of the
	file.

09-Oct-2003

	Open menu on popup-menu signal (for keyboard users).

06-Oct-2003

	Added Spanish translation (Nicolás de la Torre).

04-Oct-2003

	Marked strings as translatable and added Italian translation (Roberto
	Inzerillo).

27-Aug-2003

	Spanish translation of AppInfo (Nicolás de la Torre).

26-Aug-2003

	Added 'Messages' directory for translations (requested by Nicolás de la Torre).

26-Jul-2003

	Hide scrollbar when not in use.

23-Jul-2003 (Release 1.9.3)

	Group actions for undo and redo.
	Removed Process minibuffer.
	Cope slightly better with binary data.

22-Jul-2003

	Added Search-and-Replace toolbar button. Added section on search and replace
	to the manual.
	Added Spacing section to the options box, to control the line spacings,
	margins and indents.

20-Jul-2003

	Support older versions of pygtk (reported by Alastair Porter).

19-Jul-2003 (Release 1.9.2)

	Added stock items to menus.
	Added menu items for Cut, Copy and Paste.
	Added Search and Replace.
	Added automatic indentation feature (on by default).

04-Jul-2003

	Added italian translation in AppInfo.xml (Yuri Bongiorno)

16-May-2003 (Release 1.9.1)

	New icon (Geoff Youngs).

10-Feb-2003

	Upgraded to cope with pygtk-1.99.15.

02-Feb-2003

	Fixed problem with wheel mouse scrolling (Stuart Langridge).

29-Jan-2003

	Make wheel mouse scrolling work (requested by Albert Wagner).

21-Jan-2003

	Fix warning message when opening minibuffer (reported by joehill).

02-Sep-2002 (Release 1.9.0)

	Process minibuffer defaults to previous command.

28-Jul-2002

	Added process minibuffer.

21-Jul-2002

	Preserve permissions when saving.

19-Jul-2002

	When importing non-UTF8 text, allow the user to select an encoding to use.
